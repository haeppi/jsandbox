#[tokio::test]
async fn read_file_error() {
    let mut script = jsandbox::Script::from_string(
        r#"
        async function read_file() {
          return await Deno.readTextFile("./file.js");
        }
        "#,
    )
    .build()
    .unwrap();
    let result: Result<String, jsandbox::AnyError> = script.call("read_file", ()).await;
    let err = result.unwrap_err();

    assert!(err.to_string().contains("Requires read access to"));
}

#[tokio::test]
async fn fetch_error() {
    let mut script = jsandbox::Script::from_string(
        r#"
        async function fetch_error() {
          let resp = await fetch("https://www.google.com");
          return await resp.text();
        }
        "#,
    )
    .build()
    .unwrap();
    let result: Result<String, jsandbox::AnyError> = script.call("fetch_error", ()).await;
    let err = result.unwrap_err();

    assert!(err.to_string().contains("Requires net access to"));
}

#[tokio::test]
async fn fetch() {
    let mut script = jsandbox::Script::from_string(
        r#"
        async function fetch_success() {
          let resp = await fetch("https://www.google.com");
          return await resp.text();
        }
        "#,
    )
    .permissions(jsandbox::Permissions {
        net: jsandbox::Permissions::new_net(
            &Some(vec!["www.google.com".to_string()]),
            &None,
            false,
        )
        .unwrap(),
        ..Default::default()
    })
    .build()
    .unwrap();
    let result: String = script.call("fetch_success", ()).await.unwrap();

    assert!(result.contains("<html"));
}
