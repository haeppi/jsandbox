#[tokio::test]
async fn eval() {
    let result: u32 = jsandbox::eval("3 + 2").await.unwrap();

    assert_eq!(result, 5);
}
