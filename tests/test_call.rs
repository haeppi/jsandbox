#[tokio::test]
async fn call() {
    let mut script = jsandbox::Script::from_string("function add(a, b) { return a + b; }")
        .build()
        .unwrap();
    let result: u32 = script.call("add", (1, 2)).await.unwrap();

    assert_eq!(result, 3);
}

#[tokio::test]
async fn call_async() {
    let mut script = jsandbox::Script::from_string(
        "async function add(a, b) { return new Promise((resolve) => resolve(a + b)); }",
    )
    .build()
    .unwrap();
    let result: u32 = script.call("add", (1, 2)).await.unwrap();

    assert_eq!(result, 3);
}

#[tokio::test]
async fn call_timeout() {
    let mut script = jsandbox::Script::from_string("function timeout() { for(;;){} }")
        .build()
        .unwrap()
        .timeout(std::time::Duration::from_millis(100));
    let result: Result<String, jsandbox::AnyError> = script.call("timeout", ()).await;
    let err = result.unwrap_err();

    assert_eq!(err.to_string(), "Uncaught Error: execution terminated");
}

#[tokio::test]
async fn forgot_build() {
    let mut script = jsandbox::Script::from_string("function func() { }");
    let result: Result<String, jsandbox::AnyError> = script.call("func", ()).await;
    let err = result.unwrap_err();

    assert!(err
        .to_string()
        .contains("Script is not properly initialized"));
}

#[tokio::test]
async fn var_state() {
    let mut script = jsandbox::Script::from_string(
        "
        let val = 1;
        function get_val() { return val++; }
        ",
    )
    .build()
    .unwrap();

    let mut result: u32 = script.call("get_val", ()).await.unwrap();
    assert_eq!(result, 1);

    result = script.call("get_val", ()).await.unwrap();
    assert_eq!(result, 2);
}

#[tokio::test]
async fn func_callback() {
    let ref_value = 123_i32;
    let mut script = jsandbox::Script::from_string(
        "
        function call_my_callback() {
            return my_callback(123);
        }
        ",
    )
    .function(
        "my_callback".to_string(),
        Box::new(ref_value),
        |ref_value, value| {
            if let Some(ref_v) = ref_value.downcast_ref::<i32>() {
                assert_eq!(value, *ref_v);
            }

            Ok(jsandbox::serde_json::to_value(789)?)
        },
    )
    .build()
    .unwrap();

    let result: u32 = script.call("call_my_callback", ()).await.unwrap();
    assert_eq!(result, 789);
}
