mod args;
mod script;

pub use deno_core::serde_json;
pub use script::*;
